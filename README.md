# dell-chrome-box

#### 介绍
戴尔chrome book bios 脚本镜像

#### 软件架构
软件架构说明


#### 使用说明

执行下面这句代码
`
cd; curl -LO https://gitee.com/mrchromebox/dell/raw/master/firmware-util.sh  && sudo bash firmware-util.sh
`

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


有水管的同学需要可以迁移rom文件的通知我
rom 文件放入bios目录即可


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
